<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "Admin",
                "email" => "admin@admin.com",
                "password" => bcrypt('password'),
            ],
        ];

        foreach ($data as $n => $item) {
            try {
                $user = User::updateOrCreate(
                    [
                        "email" => $item['email'],
                    ],
                    $item
                );
            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }
}
